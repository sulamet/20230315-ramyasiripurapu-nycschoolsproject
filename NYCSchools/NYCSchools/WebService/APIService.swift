

import Foundation
class APIService :  NSObject {
    
    private let schoolUrl = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
    private let satUrl = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!

    
    func apiToGetSchoolData(completion : @escaping ([SchoolData]) -> ()){
        
        URLSession.shared.dataTask(with: schoolUrl) { (data, urlResponse, error) in
            if let data = data {
                let jsonDecoder = JSONDecoder()
                
                let schoolData = try! jsonDecoder.decode([SchoolData].self, from: data)
            
                    completion(schoolData)
            }
            
        }.resume()
    }
    
    func apiToGetSATData(completion : @escaping ([SatData]) -> ()){
        
        URLSession.shared.dataTask(with: satUrl) { (data, urlResponse, error) in
            if let data = data {
                let jsonDecoder = JSONDecoder()
                
                let satData = try! jsonDecoder.decode([SatData].self, from: data)
            
                    completion(satData)
            }
            
        }.resume()
    }
    
}
